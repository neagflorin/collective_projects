'''
primeste un fisier ca parametru de intrare
returneaza o lista de tuplets  de forma (timp,comanda)
'''

def parse_file(file_path):
    f=None
    rez = []
    try:
        f=open(file_path,'r')

        for line in f:
            l=line.split(":")
            time=l[1][1:]
            aux=l[2].split()
            cmd=aux[0][2:]
            t=time,cmd
            rez.append(t)

    except IOError:
        print "Fisierul nu poate fi deschis!"
        return -1
    if f!= None:
        f.close()
    return rez

def test_parse_file():
    assert(parse_file("teste.txt")==[('1390389962','cd'),('1390390988', 'cdc'),('1390391149', 'python')])
    assert(parse_file("test1.txt")==[('1390391981', 'sudo'), ('1390392004', 'sudo'), ('1390392179', 'yum'), ('1390392195', 'yum'), ('1390392198', 'sudo'), ('1390392215', 'pip')])
    assert(parse_file("inexistent.txt")==-1)
    print 'teste ok'

test_parse_file()