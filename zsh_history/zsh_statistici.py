import parse_file

def get_commands(list):
    cmds = {}
    for data, cmd in list:
        if cmd not in cmds:
            cmds[cmd] = 1
        else:
            cmds[cmd] += 1
    most_cmds = sorted(cmds.items(),key = lambda x:x[1],reverse=True)
    return most_cmds
