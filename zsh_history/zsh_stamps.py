import parse_file

def get_most_commands(list):
    commands = []
    for data, cmd in list:
        commands.append(cmd)
    most_commands = []
    for c in set(commands):
        t = tuple((c,commands.count(c)))
        most_commands.append(t)
    most_commands = sorted(most_commands,key=lambda x:x[1],reverse=True)

    return most_commands[:60]
